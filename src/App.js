import React from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';
const base_url = {
  url: 'http://35.240.195.198:8085/'
};

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      airlineName: '',
      ip: '',
      port: '',
      allNodes: []
    };
    this.postData = this.postData.bind(this);
    this.getData = this.getData.bind(this);
    this.deleteNode = this.deleteNode.bind(this);
  }

  componentDidMount() {
    this.getData();
  }

  postData() {
    axios.get(`${base_url.url}/monitor/register/${this.state.airlineName}/${this.state.ip}/${this.state.port}`)
        .then((res) => {
          if(res.status === 200) {
            this.getData();
          }
        })
        .catch((err) => {
          console.log(err);
        })
  }

  getData() {
    axios.get(`${base_url.url}/monitor/get/list/nodes`)
        .then((res) => {
          if(res.status === 200) {
            this.setState({allNodes: res.data.listOfNodes});
          }
        })
        .catch((err) => {
          console.log(err);
        })
  }

  deleteNode(airlineName) {
    axios.get(`${base_url.url}/monitor/delete/node/${airlineName}`)
        .then((res) => {
          if(res.status === 200) {
            setTimeout(() => {
              this.getData();
            }, 1500)
          }
        })
        .catch((err) => {
          console.log(err);
        })
  }

  render() {
    return (
        <div>
          <div className="col-5">
            <form>
                <div className="form-group">
                    <label htmlFor="airlineNo">Airline No. :</label>
                    <input type="text" className="form-control" id="airlineNo" value={this.state.airlineName} onChange={(e) => this.setState({airlineName: e.target.value})} />
                </div>
                <div className="form-group">
                    <label htmlFor="ipno">IP :</label>
                    <input type="text" className="form-control" id="ipno" value={this.state.ip} onChange={(e) => this.setState({ip: e.target.value})} />
                </div>
                <div className="form-group">
                    <label htmlFor="portno">Port No. :</label>
                    <input type="text" className="form-control" id="portno" value={this.state.port} onChange={(e) => this.setState({port: e.target.value})} />
                </div>
              <input type="button" className="btn btn-primary mb-3" value="Submit" onClick={this.postData} />
            </form>
          </div>
          <div className="col-6">
            <table className="table">
              <thead>
              <tr>
                <th scope="col"></th>
                <th scope="col">Airline</th>
                <th scope="col">IP</th>
                <th scope="col">Port No.</th>
              </tr>
              </thead>
              <tbody>
            {
              this.state.allNodes.map((item, i) => {
                return (
                    <tr key={i}>
                      <th scope="row">{i}</th>
                      <td>{item.name}</td>
                      <td>{item.ip}</td>
                      <td>{item.port}</td>
                      <td><input type="button" value="Delete" onClick={this.deleteNode.bind(null, item.name)} /></td>
                    </tr>
                )
              })
            }
              </tbody>
            </table>
          </div>
        </div>

    )
  }
}
export default App;
